package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.chess.RulesOfGame;
import pl.edu.pwsztar.domain.converter.Converter;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.dto.MovementDto;
import pl.edu.pwsztar.service.ChessService;

@Service
public class ChessServiceImpl implements ChessService {

    private Converter<MovementDto, FigureMoveDto> converter;

    private RulesOfGame bishop;
    private RulesOfGame king;
    private RulesOfGame knight;
    private RulesOfGame pawn;
    private RulesOfGame queen;
    private RulesOfGame rock;

    @Autowired
    public ChessServiceImpl(@Qualifier("Bishop") RulesOfGame bishop,
                            @Qualifier("King") RulesOfGame king,
                            @Qualifier("Knight") RulesOfGame knight,
                            @Qualifier("Pawn") RulesOfGame pawn,
                            @Qualifier("Queen") RulesOfGame queen,
                            @Qualifier("Rock") RulesOfGame rock,
                            Converter<MovementDto, FigureMoveDto> converter) {

        this.bishop = bishop;
        this.king = king;
        this.knight = knight;
        this.pawn = pawn;
        this.queen = queen;
        this.rock = rock;
        this.converter = converter;
    }

    private boolean checkMovement(FigureMoveDto figureMoveDto, MovementDto movementDto) {
        switch (figureMoveDto.getType()) {
            case KING:
                return king.isCorrectMove(movementDto.getxStart(), movementDto.getyStart(), movementDto.getxStop(), movementDto.getyStop());
            case PAWN:
                return pawn.isCorrectMove(movementDto.getxStart(), movementDto.getyStart(), movementDto.getxStop(), movementDto.getyStop());
            case ROCK:
                return rock.isCorrectMove(movementDto.getxStart(), movementDto.getyStart(), movementDto.getxStop(), movementDto.getyStop());
            case QUEEN:
                return queen.isCorrectMove(movementDto.getxStart(), movementDto.getyStart(), movementDto.getxStop(), movementDto.getyStop());
            case BISHOP:
                return bishop.isCorrectMove(movementDto.getxStart(), movementDto.getyStart(), movementDto.getxStop(), movementDto.getyStop());
            case KNIGHT:
                return knight.isCorrectMove(movementDto.getxStart(), movementDto.getyStart(), movementDto.getxStop(), movementDto.getyStop());
            default:
                return false;
        }
    }

    @Override
    public boolean isCorrectMove(FigureMoveDto figureMoveDto) {
        MovementDto movementDto = converter.convert(figureMoveDto);
        return checkMovement(figureMoveDto, movementDto);
    }
}
