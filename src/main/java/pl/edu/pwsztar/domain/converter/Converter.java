package pl.edu.pwsztar.domain.converter;

@FunctionalInterface
public interface Converter<T,F> {
    T convert(F from);
}
