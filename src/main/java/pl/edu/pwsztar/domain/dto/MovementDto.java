package pl.edu.pwsztar.domain.dto;

public class MovementDto {
    private final int xStart;
    private final int xStop;
    private final int yStart;
    private final int yStop;

    public MovementDto(int xStart, int yStart, int xStop, int yStop) {
        this.xStart = xStart;
        this.xStop = xStop;
        this.yStart = yStart;
        this.yStop = yStop;
    }

    public int getxStart() {
        return xStart;
    }

    public int getxStop() {
        return xStop;
    }

    public int getyStart() {
        return yStart;
    }

    public int getyStop() {
        return yStop;
    }
}
